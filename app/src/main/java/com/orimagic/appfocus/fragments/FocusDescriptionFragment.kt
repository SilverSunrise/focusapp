package com.orimagic.appfocus.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.orimagic.appfocus.R
import kotlinx.android.synthetic.main.fragment_focus_description.view.*


class FocusDescriptionFragment : Fragment() {
    var displayId: String? = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_focus_description, container, false)
        displayId = arguments?.getInt("key").toString()

        when (displayId) {
            "0" -> {
                view.iv_descriptionIcon.setImageResource(R.drawable.mind_reading_focus_icon)
                view.tv_description_focusName.text = "The Mind Reading Card Trick"
                view.tv_description.text =
                    "1.  Before you begin, write down a prediction on a piece of paper (i.e. 10 of Hearts) and seal it in an envelope. Then, take your deck of cards and secretly place the 10 of Hearts on top.\n\n" +
                            "2.  Hand the envelope to an audience member and instruct them not to open it.\n\n" +
                            "3.  Fan the cards in your hand and show the audience they’re all mixed up. If someone asks to shuffle the deck. Don’t panic! Once they’ve shuffled, fan through the cards again to verify they’re properly shuffled. While you do this, locate the 10 of Hearts and cut it back to the top of the deck.\n\n" +
                            "4.  Now, you will get them to select the 10 of Hearts using a super easy self-working card force known as the Cross Cut Force.\n\n" +
                            "5.  The Dapper Deck Playing Cards from Vanishing Inc. are used to before an easy card trick with the cross cut force\n\n" +
                            "6.  Place the deck face down on the table and invite an audience member to cut the deck in half and place the cards to their right.\n\n" +
                            "7.  You then immediately pick up the original bottom half on the left, turn it sideways and place it on top of the cards to the right.\n\n" +
                            "8.  For this force to be effective, you’ll now need to let a few moments pass so your audience forgets which half came from where.\n\n" +
                            "9.  To do this, look up from the deck of cards and begin recapping how you used a regular deck of cards that was fairly shuffled. Remind your volunteer that they could have cut anywhere but chose to “cut here.”\n\n" +
                            "10.  As you say “cut here” pick up the sideways top half and then point to the top card of the bottom half (the Ten of Hearts) and say “and you selected this card.”\n\n" +
                            "11.  Have the volunteer turn over the selected card and then reveal it matches your prediction!"
            }
            "1" -> {
                view.iv_descriptionIcon.setImageResource(R.drawable.impossible_three_card_focus_icon)
                view.tv_description_focusName.text = "The Impossible Three Card Trick"
                view.tv_description.text =
                    "1.  Remove the Ace of Spades, Queen of Hearts and Ace of Clubs from a deck of cards and place them on the table in that order left to right.\n\n" +
                            "2.  Turn your back and tell your spectator to simply think of 1 of the 3 cards.\n\n" +
                            "3.  Then, to secretly tell the other audience members what card they mentally selected, instruct them to pick up the 2 cards they DIDN’T select and swap their positions. (i.e. If they chose the Ace of Clubs, they would swap the Queen of Hearts and Ace of Spades).\n" +
                            "4.  Once this is complete, have your spectator turn all 3 cards face down.\n\n" +
                            "5.  Turn back around and have them mix all 3 cards on the table casino style. As they do this, carefully follow the center card.\n\n" +
                            "6.  Once they’re done shuffling, flip over the card you followed. If this card is the Queen of Hearts (the original center card), then this was their selected card. If it is an Ace of Spades, then their mental selection was the Ace of Clubs. And, if it’s an Ace of Clubs, then they selected the Ace of Spades.\n\n" +
                            "7.  Flip over the other 2 cards and tell them to think about their card. After pretending to read their mind, push their selected card forward."
            }
            "2" -> {
                view.iv_descriptionIcon.setImageResource(R.drawable.upside_down_card_focus_icon)
                view.tv_description_focusName.text = "The Upside Down Card"
                view.tv_description.text =
                    "1.  Before you begin, secretly turn over the bottom card of your deck so it’s the only card facing up.\n\n" +
                            "2.  Spread the deck and allow the spectator to select a card (being careful not to expose the face up bottom card). Tell them to remember the card and show everyone without letting you see.\n\n" +
                            "3.  While they do this, casually turn the deck over in your hands. This leaves all of the deck face up in your left hand, except the top card.\n\n" +
                            "4.  Keeping the deck tight to avoid exposing the fact the cards are reversed, place the spectator’s selected card face down into the middle of the deck.\n\n" +
                            "5.  Put the cards behind your back and exclaim you’re going to find their card without looking. Secretly turn over the top card so it’s facing up.\n\n" +
                            "6.  Bring out the face up deck and fan through until you come across the only face down card. Slowly reveal it is the selected card."
            }
            "3" -> {
                view.iv_descriptionIcon.setImageResource(R.drawable.mind_reading_with_friends_focus_icon_1)
                view.tv_description_focusName.text = "Mind Reading With Friends"
                view.tv_description.text =
                    "1.  Lay down 3 rows of 3 cards. They can be any cards. However, we would suggest placing a 5 of Diamonds in the upper left corner for reasons that will become clearer later.\n\n" +
                            "2.  Tell your audience that you’re going to leave the room and instruct them to all agree on one of the playing cards while you’re gone.\n\n" +
                            "3.  Once you return to the room, explain that you are going to magically discover what card they selected.\n\n" +
                            "4.  Your assistant will then slowly point to each card with a pencil in front of anyone without saying anything out loud.\n\n" +
                            "5.  The secret to this trick lies in how the first card (the 5 of Diamonds in this case) is touched. Imagine it’s broken up into a 3 x 3 grid. For example, if they point to the top right of the card (right on the top right pip), that means the selected card was the right card of the top row (Ace of Clubs in the pictured example). Or, if they point to the middle of the bottom ? of the card (below the middle pip and in-between the two outer pips), then the selected card is the middle card of the bottom row (6 of Spades).\n\n" +
                            "6.  Once the first card has been touched, you’ll know which card the audience selected and the rest is just showmanship. Your assistant can touch the other cards anywhere.\n\n" +
                            "7.  Once all the cards have been touched, dramatically reveal the selected card."
            }
            "4" -> {
                view.iv_descriptionIcon.setImageResource(R.drawable.do_as_i_do_focus_icon)
                view.tv_description_focusName.text = "Do as I Do"
                view.tv_description.text =
                    "1.  This trick requires two full decks of cards (without Jokers). It’s important that no cards are missing. We would also suggest using two types of decks.\n\n" +
                            "2.  Bring out both decks of cards and allow the spectator to choose one.\n\n" +
                            "3.  Instruct the spectator to follow everything you do as you begin to freely shuffle and cut the cards.\n\n" +
                            "4.  While finishing up your last shuffle, quickly peek at the bottom card of your deck and memorize it. (we’ll use the Ace of Spades for this example).\n\n" +
                            "5.  Exchange decks with your spectator so they can be sure you’re not using a trick deck or sneaky sleight of hand.\n\n" +
                            "6.  After you exchange, instruct your spectator to cut off half the deck and place the cards to their right.\n\n" +
                            "7.  Have them take the top card from the pile to their left, remember the card and place it on top of the cards to their right without showing you.\n\n" +
                            "8.  Then, instruct them to pick up the pack on the left and place all these cards on top of the deck to their right. While this looks like it fairly buries their selected card in the middle of the deck, it actually secretly positions it next to your memorized key card from earlier (Ace of Spades).\n\n" +
                            "9.  As they’re doing this, you’re completing the same actions. Except, you do not need to actually remember the card you select. Just continue repeating your key card (Ace of Spades) in your head.\n\n" +
                            "10.  Exchange decks again.\n\n" +
                            "11.  Tell your spectator to look for their card in their deck and pull it out face down. You will do the same, pretending to look for the card you seemingly just randomly selected. But, in reality, you are looking for the Ace of Spades. The card to its right will be their selection. Pull it out face down.\n\n" +
                            "12.  Count to 3 and turn over both cards to show that you’ve both picked a perfect match."
            }
        }

        return view

    }


}