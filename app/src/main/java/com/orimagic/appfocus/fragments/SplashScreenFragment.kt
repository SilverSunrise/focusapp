package com.orimagic.appfocus.fragments

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.orimagic.appfocus.R


class SplashScreenFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_splash_screen, container, false)
        Handler().postDelayed({
            val mainFragment = MainFragment()
            fragmentManager?.beginTransaction()?.replace(R.id.fragment_container, mainFragment)
                ?.commit()
        }, SPLASH_SCREEN_TIME)


        return view
    }

    companion object {
        private const val SPLASH_SCREEN_TIME: Long = 3000
    }
}