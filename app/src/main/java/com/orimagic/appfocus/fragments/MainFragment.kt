package com.orimagic.appfocus.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.orimagic.appfocus.adapter.Adapter
import com.orimagic.appfocus.model.Model
import com.orimagic.appfocus.R

class MainFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_main, container, false)
        val arrayList = ArrayList<Model>()

        arrayList.add(
            Model(
                "The Mind Reading Card Trick",
                R.drawable.icon_focus
            )
        )

        arrayList.add(
            Model(
                "The Impossible Three Card Trick",
                R.drawable.icon_focus
            )
        )

        arrayList.add(
            Model(
                "The Upside Down Card",
                R.drawable.icon_focus
            )
        )

        arrayList.add(
            Model(
                "Mind Reading With Friends",
                R.drawable.icon_focus
            )
        )

        arrayList.add(
            Model(
                "Do as I Do",
                R.drawable.icon_focus
            )
        )

        val myAdapter = Adapter(arrayList, context!!)
        val mainMenu = view.findViewById(R.id.recyclerView) as RecyclerView
        mainMenu.layoutManager = LinearLayoutManager(context!!)
        mainMenu.adapter = myAdapter

        return view
    }

}