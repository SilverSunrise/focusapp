package com.orimagic.appfocus.communicator

interface Communicator {
    fun passData(id: Int)
}