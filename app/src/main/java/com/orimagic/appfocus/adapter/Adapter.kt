package com.orimagic.appfocus.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.orimagic.appfocus.communicator.Communicator
import com.orimagic.appfocus.model.Model
import com.orimagic.appfocus.R
import kotlinx.android.synthetic.main.focus_model.view.*

class Adapter(private val arrayList: ArrayList<Model>, private val context: Context) :
    RecyclerView.Adapter<Adapter.ViewHolder>() {

    private lateinit var communicator: Communicator

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(model: Model) {
            itemView.tv_focusName.text = model.title
            itemView.iv_iconFocus.setImageResource(model.image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.focus_model, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        communicator = context as Communicator
        holder.bindItems(arrayList[position])

        holder.itemView.setOnClickListener {
            communicator.passData(position)
        }
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

}