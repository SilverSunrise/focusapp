package com.orimagic.appfocus

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.orimagic.appfocus.communicator.Communicator
import com.orimagic.appfocus.fragments.FocusDescriptionFragment
import com.orimagic.appfocus.fragments.MainFragment
import com.orimagic.appfocus.fragments.SplashScreenFragment


class MainActivity : AppCompatActivity(), Communicator {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val splashScreenFragment = SplashScreenFragment()
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, splashScreenFragment)
            .commit()
    }

    override fun passData(id: Int) {
        val bundle = Bundle()
        bundle.putInt("key", id)

        val transaction = this.supportFragmentManager.beginTransaction()
        val focusDescriptionFragment = FocusDescriptionFragment()

        focusDescriptionFragment.arguments = bundle
        transaction.replace(R.id.fragment_container, focusDescriptionFragment)
        transaction.commit()
    }

    override fun onBackPressed() {
        val mainFragment = MainFragment()
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, mainFragment)
            .commit()
    }
}


